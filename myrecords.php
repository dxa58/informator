<?php
session_start();
require_once 'lib/class/Record.php';
if (!isset($_SESSION['username']))
{
	$_SESSION['pleaseLogin'] = true;
	header('Location: index.php');
}

if($_SESSION['role']=='student')
{
	header('Location: student.php');
}
if($_SESSION['role']=='administrator')
{
	header('Location: administrator.php');
}

require_once 'lib/utility/actualURL.php';


$urlPOST = $actual_link."/services/searchrecords/";
        $curl_post_data = array(
        'field' => 'users_userID',
        'search' => $_SESSION['userID']
        );
        $curl = curl_init($urlPOST);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
        $responce = curl_exec($curl);
        $data = json_decode($responce);
        $records = array();
for ($i=0; $i<=count($data)-1;$i++)
{
        $record = new Record();
        $record->jsonDeserialize($data[$i]);
        array_push($records,$record);
}
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Evidencija zaposlenih</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen, projection, tv" />
<link rel="stylesheet" href="css/style-print.css" type="text/css" media="print" />
</head>
<body>
<div id="wrapper">
<!--   <div class="title">  
        <h1>Evidencija zaposlenih</h1>
  </div> -->
</div>
  <hr class="noscreen" />
  <div class="content">
    <div class="column-left">
      <h3>MENI</h3>
      <a href="#skip-menu" class="hidden">Skip menu</a>
      <ul class="menu">
        <li><a href="<?php echo $actual_link."/student.php";?>" >Naslovna</a></li>
        <li><a href="<?php echo $actual_link."/myrecords.php";?>" class="active">Moje evidencije</a></li>
        <li><a href="<?php echo $actual_link."/changepassword.php";?>">Promena lozinke</a></li>
        <li><a href="<?php echo $actual_link."/logout.php";?>" class="last">Odjava</a></li>
      </ul>
    </div>
    <div id="skip-menu"></div>
    <div class="column-right">
      <div class="box">
        <div class="box-top"></div>
        <div class="box-in">
                <table>
                        <tr>
                                <th>
                                Datum
                                </th>
                                <th>
                                Vreme dolaska
                                </th>
                                <th>
                                Vreme odlaska
                                </th>
                        </tr>
                        <?php foreach($records as $r): ?>
                        <tr>
                                <td class="tdright">
                                <?php echo $r->get_date(); ?>
                                </td>
                                <td class="tdright">
                                <?php echo $r->get_arrival(); ?>
                                </td>
                                <td class="tdright">
                                <?php echo $r->get_departure(); ?>
                                </td>
                        </tr>
                <?php endforeach; ?>
                </table> 
        </div>
      </div>
    </div>
    <div class="cleaner">&nbsp;</div>
  </div>
</body>
</html>