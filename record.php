<?php
session_start();
require_once 'lib/class/Record.php';
require_once 'lib/class/User.php';

if (!isset($_SESSION['username']))
{
	$_SESSION['pleaseLogin'] = true;
	header('Location: index.php');
}

if($_SESSION['role']=='professor')
{
	header('Location: professor.php');
}
if($_SESSION['role']=='student')
{
	header('Location: student.php');
}

require_once 'lib/utility/actualURL.php';

if(isset($_POST['recordID']))
{
	if($_POST['recordID'] == 0)
	{
		if(empty($_POST['departure']))
		{
			$_POST['departure'] = 'NULL';
		}

		if(strlen($_POST['arrival']) == 5)
		{
			$_POST['arrival'] = $_POST['arrival'].":00";
		}

		if(strlen($_POST['departure']) == 5)
		{
			$_POST['departure'] = $_POST['departure'].":00";
		}


		$urlPOST = $actual_link."/services/addrecord";
		$curl_post_data = array(
			'users_userID' => $_POST['users_userID'],
			'date' => $_POST['date'],
			'arrival' => $_POST['arrival'],
			'departure' => $_POST['departure'],
			'author' => 'administrator',
		);
		$curl = curl_init($urlPOST);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$result = curl_exec($curl);
		$out = json_decode($result);
		header("Location: records.php");
	}
	else
	{
		if(empty($_POST['departure']))
		{
			$_POST['departure'] = 'NULL';
		}
		$urlPOST = $actual_link."/services/editrecord";
		$curl_post_data = array(
			'recordID' => $_POST['recordID'],
			'users_userID' => $_POST['users_userID'],
			'date' => $_POST['date'],
			'arrival' => $_POST['arrival'],
			'departure' => $_POST['departure']
		);
		$curl = curl_init($urlPOST);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$result = curl_exec($curl);
		$out = json_decode($result);
		header("Location: records.php");
	}
}




if(isset($_GET['recordID']))
{
	$targetURL = $actual_link."/services/record?recordID=".$_GET['recordID'];
	$curl = curl_init($targetURL);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	$response = curl_exec($curl);
	$data = json_decode($response);
	$id = $_GET['recordID'];
	if($data[0])
	{
		$record = new Record();
		$record->jsonDeserialize($data[0]);
		$_SESSION['exists'] = true;
	}
	else
	{
		$_SESSION['exists'] = false;
	}
}
else
{
	$record = new Record();
	$id = 0;
}

	$urlPOST = $actual_link."/services/searchuser";
	$curl_post_data = array(
		'field' => "role",
		'search' => "professor"
	);
	$curl = curl_init($urlPOST);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
	$result = curl_exec($curl);
	$data = json_decode($result);
	$users = array();
	for ($i=0; $i<=count($data)-1;$i++)
	{
		$user = new User();
		$user->jsonDeserialize($data[$i]);

		array_push($users,$user);
	}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Evidencija zaposlenih</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen, projection, tv" />
<link rel="stylesheet" href="css/style-print.css" type="text/css" media="print" />

<script type="text/javascript" src="http://code.jquery.com/jquery-2.1.4.min.js"></script> 
<script src="//cdn.jsdelivr.net/webshim/1.14.5/polyfiller.js"></script>
<script>
webshims.setOptions('forms-ext', {types: 'date'});
webshims.polyfill('forms forms-ext');
$.webshims.formcfg = {
en: {
    dFormat: '-',
    dateSigns: '-',
    patterns: {
        d: "yy-mm-dd"
    }
}
};
</script>
</head>
<body>
<div id="wrapper">
</div>
  <hr class="noscreen" />
  <div class="content">
    <div class="column-left">
      <h3>ADMIN MENI</h3>
      <a href="#skip-menu" class="hidden">Skip menu</a>
      <ul class="menu">
      	<nav>
        <li><a href="<?php echo $actual_link."/administrator.php";?>">Naslovna</a></li>
        <li><a href="<?php echo $actual_link."/records.php";?>" class="active" >Evidencije</a></li>
        <li><a href="<?php echo $actual_link."/users.php";?>" >Korisnici</a></li>
        <li><a href="<?php echo $actual_link."/regularity.php";?>">Redovnost dolazaka</a></li>
        <li><a href="<?php echo $actual_link."/consecutive-absence.php";?>">Uzastopno odsustvo</a></li>
        <li><a href="<?php echo $actual_link."/changepassword.php";?>">Promena lozinke</a></li>
        <li><a href="<?php echo $actual_link."/logout.php";?>" class="last">Odjava</a></li>
    	</nav>
      </ul>
    </div>
    <div id="skip-menu"></div>
    <div class="column-right">
      <div class="box">
        <div class="box-top"></div>
        <div class="box-in">
         <h2>Evidencija</h2>
 		<?php 
 		if(isset($_GET['recordID']) && !$_SESSION['exists'])
 		{
 			echo "Evidencija sa tim ID ne postoji u bazi podataka!";
 		}
 		else
 		{	 			
 		?>
 		<form method="post" action="record.php">
		<table>
			<tr>
			<input type="text" name="recordID" value="<?php echo $id;?>" hidden="true">
	 		<td>Korisnik: </td>  
			  <td><select name="users_userID">
			  <?php
			  foreach($users as $u)
			  {  
	      	   	?>
	          <option value="<?php echo $u->get_userID(); ?>" <?php if ($u->get_userID() == $record->get_users_userID()) echo " selected"; ?>><?php echo $u->get_userID()." - ".$u->get_name()." ".$u->get_lastname(); ?> </option>          
	         	<?php    
	          } 		

 	 		?>                
              </select></td>
		  </tr>
		  <tr>
		  <td>Datum: </td>  
		  <td><input type="date" name="date" value="<?php echo $record->get_date(); ?>" required /></td>
		  </tr>
		  <tr>
		  <td>Dolazak: </td> 
		  <td><input type="time"  name="arrival" value="<?php echo $record->get_arrival(); ?>" required/></td>
		  </tr>
		  <tr>
		  <td>Odlazak: </td>
		  <td><input type="time"  name="departure" value="<?php echo $record->get_departure(); ?>"/></td>
		  </tr>
		  </table>
		  <br>
		  <input type="submit" value="Potvrdi">
		  </form>
	  	<?php }	  	?>
	  	</div>
      </div>
    </div>
    <div class="cleaner">&nbsp;</div>
  </div>
</body>
</html>