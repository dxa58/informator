<?php
session_start();

if (!isset($_SESSION['username']))
{
	$_SESSION['pleaseLogin'] = true;
	header('Location: index.php');
}

if($_SESSION['role']=='professor')
{
	header('Location: professor.php');
}
if($_SESSION['role']=='student')
{
	header('Location: student.php');
}

require_once 'lib/utility/actualURL.php';

if(isset($_POST['recordID']))
{
		$urlPOST = $actual_link."/services/deleterecord";
		$curl_post_data = array(
			'recordID' => $_POST['recordID']
		);
		$curl = curl_init($urlPOST);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$result = curl_exec($curl);
		$out = json_decode($result);
		header("Location: records.php");
}

if(isset($_POST['userID']))
{
		if($_POST['userID'] == $_SESSION['userID'])
		{
			$_SESSION['self'] = true;
			header("Location: users.php");
		}
		else
		{
			$urlPOST = $actual_link."/services/deleteuser";
			$curl_post_data = array(
				'userID' => $_POST['userID']
			);
			$curl = curl_init($urlPOST);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
			$result = curl_exec($curl);
			$out = json_decode($result);
			header("Location: users.php");
		}

}
?>