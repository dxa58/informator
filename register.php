<?php
session_start();
if(isset($_SESSION['role']))
{
	if($_SESSION['role']=='professor')
	{
		header('Location: professor.php');
	}
	if($_SESSION['role']=='administrator')
	{
		header('Location: administrator.php');
	}
	if($_SESSION['role']=='student')
	{
		header('Location: student.php');
	}
}
    
require_once 'lib/utility/actualURL.php';

  if(!empty($_POST))
  {
    
    $urlPOST = $actual_link."/services/adduser";
    $curl_post_data = array(
      'username' => $_POST['username'],
      'password' => $_POST['password'],
      'name' => $_POST['name'],
      'lastname' => $_POST['lastname'],
      'role' => 'student',
      'rank' => 'docent',  // to avoid messing with NULL, will be flushed 10 rows down
      'sex' => $_POST['sex']
    );
    $curl = curl_init($urlPOST);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
    $result = curl_exec($curl);
    $out = json_decode($result);
    if($out)
    {
        $_SESSION['created'] = true;

        $targetURL = $actual_link."/services/managerank"; //flushing student or admin ranks
        $curl = curl_init($targetURL);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);

        header("Location: index.php");
    }
    else
    {
        $_SESSION['created'] = false;
    }
    }
?>
<!DOCTYPE HTML>
 <head>
 <title>Informator</title>
 <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  <link rel="stylesheet" type="text/css" href="css/login.css">
  <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
</head> 


<div class="container">
    <div class="row">
        <div class="col-md-offset-5 col-md-3">
        	<div class="centered">
            <div class="form-login">
            <?php
            if(isset($_SESSION['created']))
            {
            	echo "<h4>Korisničko ime je zauzeto!</h4>";
            	session_unset();
            	session_destroy();
            }
            else
            	echo "<h4>Kreiraj nalog</h4>";
            ?>
            <form action="register.php" method="POST">
            <input type="text" name="username" class="form-control input-sm chat-input" placeholder="Korisničko ime" required="true" pattern=".{5,50}"   required title="5 do 50 karaktera" />
            <br>
            <input type="password" name="password" class="form-control input-sm chat-input" placeholder="Lozinka" required="true" pattern=".{5,50}"   required title="5 do 50 karaktera"/>
            <br>
            <input type="text" name="name" class="form-control input-sm chat-input" placeholder="Ime" required="true" pattern=".{2,50}"   required title="2 do 50 karaktera"/>
            <br>
            <input type="text" name="lastname" class="form-control input-sm chat-input" placeholder="Prezime" required="true" pattern=".{2,50}"   required title="2 do 50 karaktera"/>
            <br>
            Pol:<br>
            <input type="radio"  name="sex" value="male" required/> Muški<br>
            <input type="radio"  name="sex" value="female" /> Ženski<br><hr>
            <div class="wrapper">
            <span class="group-btn">     
                <input type="submit" class="btn btn-primary btn-md" value="Registruj se">
            </span>
            </div>
            </form>
            <br>
            <div class="wrapper">
            <a href="index.php">Povratak na prijavu</a>   
            </div>
            </div>
        	</div>
        </div>
    </div>
</div>
