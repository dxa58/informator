<?php
session_start();
require_once 'lib/class/User.php';
if (!isset($_SESSION['username']))
{
	$_SESSION['pleaseLogin'] = true;
	header('Location: index.php');
}

if($_SESSION['role']=='professor')
{
	header('Location: professor.php');
}
if($_SESSION['role']=='student')
{
	header('Location: student.php');
}

require_once 'lib/utility/actualURL.php';

$targetURL = $actual_link."/services/users";


$curl = curl_init($targetURL);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
$response = curl_exec($curl);
$data = json_decode($response);
$users = array();
for ($i=0; $i<=count($data)-1;$i++)
{
	$user = new User();
	$user->jsonDeserialize($data[$i]);

	array_push($users,$user);
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Evidencija zaposlenih</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen, projection, tv" />
<link rel="stylesheet" href="css/style-print.css" type="text/css" media="print" />
</head>
<body>
<div id="wrapper">
</div>
  <hr class="noscreen" />
  <div class="content">
    <div class="column-left">
      <h3>ADMIN MENI</h3>
      <a href="#skip-menu" class="hidden">Skip menu</a>
      <ul class="menu">
      	<nav>
        <li><a href="<?php echo $actual_link."/administrator.php";?>" >Naslovna</a></li>
        <li><a href="<?php echo $actual_link."/records.php";?>">Evidencije</a></li>
        <li><a href="<?php echo $actual_link."/users.php";?>" class="active">Korisnici</a></li>
        <li><a href="<?php echo $actual_link."/regularity.php";?>">Redovnost dolazaka</a></li>
        <li><a href="<?php echo $actual_link."/consecutive-absence.php";?>">Uzastopno odsustvo</a></li>
        <li><a href="<?php echo $actual_link."/changepassword.php";?>">Promena lozinke</a></li>
        <li><a href="<?php echo $actual_link."/logout.php";?>" class="last">Odjava</a></li>
        </nav>
      </ul>
    </div>
    <div id="skip-menu"></div>
    <div class="column-right">
      <div class="box">
        <div class="box-top"></div>
        <div class="box-in">
          <h2>Korisnici</h2><br>
           		<form action="user.php">
					<input type="submit" value="Novi korisnik" />
				</form>
         	<br><br>
          <?php if(isset($_SESSION['self']) && $_SESSION['self'])
          {
          	echo "<ul><li>Nije moguće obrisati svoj nalog!</li></ul><br><br>";
          	$_SESSION['self'] = false;
          }
          ?>
          <table >
				<tr>
					<th>
					ID
					</th>
					<th>
					Korisničko ime
					</th>
					<th>
					Ime
					</th>
					<th>
					Prezime
					</th>
					<th>
					Uloga
					</th>
					<th>
					Zvanje
					</th>
					<th>
					Pol
					</th>
					<th>
					Izmeni
					</th>
					<th>
					Obrisi
					</th>
				</tr>
			<?php foreach($users as $r): ?>
				<tr>
					<td>
					<?php echo $r->get_userID(); ?>
					</td>
					<td class="tdright">
					<?php echo $r->get_username(); ?>
					</td>
					<td class="tdright">
					<?php echo $r->get_name(); ?>
					</td>
					<td class="tdright">
					<?php echo $r->get_lastname(); ?>
					</td>
					<td class="tdright">
					<?php echo $r->get_role(); ?>
					</td>
					<td class="tdright">
					<?php echo $r->get_rank(); ?>
					</td>
					<td class="tdright">
					<?php echo $r->get_sex(); ?>
					</td>
					<td class="tdright">
                        <form name="edit<?php echo $r->get_userID(); ?>" method="GET" action="user.php">
							<input type="hidden" name="userID" value="<?php echo $r->get_userID(); ?>"/>
							<input type="Button" value="Izmeni" onclick="document.edit<?php echo $r->get_userID(); ?>.submit()"/>
						</form>
					</td>
					<td class="tdright">
						<form name="delete<?php echo $r->get_userID(); ?>" method="POST" action="delete.php">
							<input type="hidden" name="userID" id="id" value="<?php echo $r->get_userID(); ?>"/>
							<input type="Button" value="Obriši" onclick="document.delete<?php echo $r->get_userID(); ?>.submit()"/>
						</form>
					</td>
				</tr>
			<?php endforeach; ?>
			</table> 
        </div>
      </div>
    </div>
    <div class="cleaner">&nbsp;</div>
  </div>
</body>
</html>
