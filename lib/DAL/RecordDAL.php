<?php
require_once 'DAL.php';
require_once __DIR__ . '/../class/Record.php';
require_once 'CommonDatabaseMethods.php';
require_once __DIR__ . '/../class/User.php';
require_once 'DALQueryResults.php';

class RecordDAL extends DAL implements CommonDatabaseMethods
{
	function __construct()
	{
		parent::__construct();
	}

	public function GetAll()
	{
		$results = $this->ExecuteQuery("SELECT `recordID`,`users_userID`,DATE_FORMAT(date, '%d.%m.%Y') as date,`arrival`,`departure`,`author`,DATE_FORMAT(lastEdit, '%d.%m.%Y %H:%i:%S') as lastEdit FROM RECORDS  ORDER BY `recordID` DESC");

		$recordResults = array();

		foreach ($results as $r)
		{
			$recordResult = new Record();

			$recordResult->set_recordID($r->recordID);
			$recordResult->set_users_userID($r->users_userID);
			$recordResult->set_date($r->date);
			$recordResult->set_arrival($r->arrival);
			$recordResult->set_departure($r->departure);
			$recordResult->set_author($r->author);
			$recordResult->set_lastEdit($r->lastEdit);
			$recordResults[] = $recordResult;
		}

		return $recordResults;
	}

	public function AddOne($object)
	{

		$object->ValidateFields();

		if($object->get_departure() == 'NULL')
		{
			$departure = ' NULL ';
		}
		else
		{
			$departure = "'".$object->get_departure()."' ";
		}

		$query = 
		"INSERT INTO RECORDS 
		VALUES
		(
		NULL,'" //recordID
		.$object->get_users_userID()."','"
		.$object->get_date()."','"
		.$object->get_arrival()."',"
		.$departure.",'"
		.$object->get_author()."', 
		NOW()   	);";  //lastEdit


		return $this->ExecuteQuery($query);
	}

	public function EditOne($object)
	{

		$object->ValidateFields();

		if($object->get_departure() == 'NULL')
		{
			$departure = 'departure = NULL ,';
		}
		else
		{
			$departure = "departure ='".$object->get_departure()."', ";
		}

		$query = 
		"UPDATE RECORDS SET 
		 users_userID = '".$object->get_users_userID()."', 
		 date ='".$object->get_date()."', 
		 arrival ='".$object->get_arrival()."', "
		 .$departure." 
		 lastEdit = NOW() 
		 WHERE
		 recordID = ".$object->get_recordID().";";

		 return $this->ExecuteQuery($query);
	}

	public function DeleteOne($object)
	{

		$object->ValidateFields();

		$query =
			"DELETE FROM RECORDS
			 WHERE 
			 recordID = ".$object->get_recordID();
		return $this->ExecuteQuery($query);
	}

	public function GetOne($object)
	{

		$recordResult = new Record();
		$recordResult->set_recordID($object);

		$results = $this->ExecuteQuery("
			SELECT *
			FROM
			RECORDS
			WHERE
			recordID =".$recordResult->get_recordID());

		if(count($results)>0)
		{
			$r = $results[0];

			$recordResult = new Record();
			$recordResult->set_recordID($r->recordID);
			$recordResult->set_users_userID($r->users_userID);
			$recordResult->set_date($r->date);
			$recordResult->set_arrival($r->arrival);
			$recordResult->set_departure($r->departure);
			$recordResult->set_author($r->author);
			$recordResult->set_lastEdit($r->lastEdit);

			return $recordResult;
		}

			return false;
	}

	public function SearchRecord($field, $search)
	{
		$query= "
			SELECT `recordID`,`users_userID`,DATE_FORMAT(date, '%d.%m.%Y') as date,`arrival`,`departure`,`author`,DATE_FORMAT(lastEdit, '%d.%m.%Y %H:%i:%S') as lastEdit
			FROM
			RECORDS
			WHERE 
			".$field." 
			LIKE 
			'%".$search."%'
			ORDER BY date DESC";
		$results = $this->ExecuteQuery($query);

		$recordResults = array();

		foreach ($results as $r)
		{
			$recordResult = new Record();

			$recordResult->set_recordID($r->recordID);
			$recordResult->set_users_userID($r->users_userID);
			$recordResult->set_date($r->date);
			$recordResult->set_arrival($r->arrival);
			$recordResult->set_departure($r->departure);
			$recordResult->set_author($r->author);
			$recordResult->set_lastEdit($r->lastEdit);
			$recordResults[] = $recordResult;
		}

		if(empty($recordResult)) 
			return false;

			return $recordResults;
	}


	//check if record exists where date = today and departure = null
	//restrict
	public function AddArrival($object)
	{

		
		$object->ValidateFields();	

		$query = 
		"SELECT `recordID`,`users_userID`,DATE_FORMAT(date, '%d.%m.%Y') as date,`arrival`,`departure`,`author`,DATE_FORMAT(lastEdit, '%d.%m.%Y %H:%i:%S') as lastEdit 
		FROM 
		RECORDS 
		WHERE 
		users_userID = ".$object->get_users_userID()." 
		AND 
		date = CURRENT_DATE() 
		AND 
		departure IS NULL ";

		$results = $this->ExecuteQuery($query);

		if(empty($results))
		{
			$query = 
			"INSERT INTO RECORDS 
			VALUES
			(
			NULL,
			".$object->get_users_userID().", 
			CURRENT_DATE(), 
			CURRENT_TIME(), 
			NULL, 
			'".$object->get_author()."', 
			NOW());";

			return $this->ExecuteQuery($query);
		}

		return false;
		

		
	}


	//check if record exists where date = today and departure = null
	//update departure & lastEdit
	public function AddDeparture($object)
	{
		
		$object->ValidateFields();	

		$query = 
		"SELECT `recordID`,`users_userID`,DATE_FORMAT(date, '%d.%m.%Y') as date,`arrival`,`departure`,`author`,DATE_FORMAT(lastEdit, '%d.%m.%Y %H:%i:%S') as lastEdit 
		FROM 
		`records` 
		WHERE 
		`users_userID` = ".$object->get_users_userID()." 
		AND 
		`date` = CURRENT_DATE() 
		AND 
		`departure` IS NULL ";

		$results = $this->ExecuteQuery($query);

		if (!empty(($results[0]->recordID)));
		{
			$query = 
			"UPDATE RECORDS SET 
			 departure = CURRENT_TIME() ,
			 lastEdit = NOW() 
			 WHERE
			 recordID = ".$results[0]->recordID.";";

			 return $this->ExecuteQuery($query);
		}
		return false;
	}


	public function GetAllPresent()
	{
		$results = $this->ExecuteQuery(
			"SELECT 
			*
			FROM RECORDS INNER JOIN USERS
			 ON users_userID = userID 
			WHERE `date` = CURRENT_DATE() 
			AND 
			`departure` IS NULL ;");

		$recordResults = array();
		$userResults = array();
		$mixedResults = array();

		foreach ($results as $r)
		{
			$recordResult = new Record();

			$recordResult->set_recordID($r->recordID);
			$recordResult->set_users_userID($r->users_userID);
			$recordResult->set_date($r->date);
			$recordResult->set_arrival($r->arrival);
			$recordResult->set_departure($r->departure);
			$recordResult->set_author($r->author);
			$recordResult->set_lastEdit($r->lastEdit);


			$userResult = new User();

			$userResult->set_userID($r->userID);
			$userResult->set_username($r->username);
			$userResult->set_password($r->password);
			$userResult->set_name($r->name);
			$userResult->set_lastname($r->lastname);
			$userResult->set_rank($r->rank);
			$userResult->set_role($r->role);
			$userResult->set_sex($r->sex);

			$mixedResult = array(
				0 => $recordResult,
				1 => $userResult);
			$mixedResults[] = $mixedResult;

		}

		return $mixedResults;
	}

	public function Present($object)
	{

		$object->ValidateFields();

		$query = 
		"SELECT `recordID`,`users_userID`,DATE_FORMAT(date, '%d.%m.%Y') as date,`arrival`,`departure`,`author`,DATE_FORMAT(lastEdit, '%d.%m.%Y %H:%i:%S') as lastEdit 
		FROM 
		RECORDS 
		WHERE 
		users_userID = ".$object->get_userID()." 
		AND 
		date = CURRENT_DATE() 
		AND 
		departure IS NULL ";

		$results = $this->ExecuteQuery($query);

		if(empty($results))
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	public function Regularity($dateStart,$dateEnd,$userID)
	{
		$query = 
		"SELECT COUNT(DISTINCT date) AS 'Total', MAX(DATE_FORMAT(date, '%d.%m.%Y')) AS 'Last' 
		 FROM RECORDS
		  WHERE date >= '".$dateStart."'  
		  AND date <= '".$dateEnd."'  
		  AND users_userID = ".$userID;

		$results =  $this->ExecuteQuery($query);

		$data = array(

			'Total' => $results[0]->Total,
			'Last' => $results[0]->Last

			);
		return $data;
	}


	//calculate consecutive workdays the employee was absent

	public function Elapsed($userID)
	{
		$query = 
		"SELECT 5 * (DATEDIFF(CURRENT_DATE(), MAX(date)) DIV 7) + MID('0123444401233334012222340111123400012345001234550', 7 * WEEKDAY(MAX(date)) + WEEKDAY(CURRENT_DATE()) + 1, 1) as 'Elapsed', MAX(DATE_FORMAT(date, '%d.%m.%Y')) as 'Last' from records where users_userID = ".$userID;

		$results =  $this->ExecuteQuery($query);

		$data = array(

			'Elapsed' => $results[0]->Elapsed,
			'Last' => $results[0]->Last
			);
		return $data;
	}
}
?>