<?php
require_once __DIR__ . '/../utility/dbsettings.php';
require_once 'DALQueryResults.php';


class DAL
{
	protected $_connection;
	protected $_dbSettings;

	public function __construct()
	{
		$this->_dbSettings = new dbSettings();
	}

	private function ConnectToDatabase()
	{
		$this->_connection = mysqli_connect(
					$this->_dbSettings->get_dbserver(),
					$this->_dbSettings->get_dbuser(),
					$this->_dbSettings->get_dbpassword(),
					$this->_dbSettings->get_database())
			or die ("Database connection failed. Aborting.");

		return $this->_connection;
	}

	private function DisconnectFromDatabase()
	{
		mysqli_close($this->_connection);
	}

	protected function ExecuteQuery($sqlQuery)
	{
		$this->ConnectToDatabase();
		$queryResults = mysqli_query($this->_connection, $sqlQuery);
		if ($queryResults)
		{
			if (strpos($sqlQuery,'SELECT') === false) //there are result and it wasn't a select statement
			{
				return true;
			}
		}
		else
		{
			if (strpos($sqlQuery,'SELECT') === false) //there weren't results and it wasn't a select statement
			{
				return false;
			}
			else // it was a select statement but there are no results
			{
				return null;
			}
		}

		$results = array();

		while ($row = mysqli_fetch_array($queryResults, MYSQLI_ASSOC))
		{
			$queryResultsDAL = new DALQueryResults(); 
			foreach ($row as $key=>$value)
			{
				$queryResultsDAL->$key = $value;
			}
			$results[] = $queryResultsDAL;
		}
		$this->DisconnectFromDatabase();

		return $results; // it was a relect statement, there were results, here they are as an array of objects
	}
}
?>