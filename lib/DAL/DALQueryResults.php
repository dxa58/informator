<?php

class DALQueryResults
{
	protected $_results = array();

	public function __construct(){}

	public function __set($key,$value) //ooh are those magic methods?
	{
		$this->_results[$key] = $value;
	}

	public function __get($key) // so magical
	{
		if (isset($this->_results[$key]))
		{
			return $this->_results[$key];
		}
		else
		{
			return null;
		}
	}
}
?>