<?php
require_once 'DAL.php';
require_once __DIR__ . '/../class/User.php';
require_once 'CommonDatabaseMethods.php';

class UserDAL extends DAL implements CommonDatabaseMethods
{
	function __construct()
	{
		parent::__construct();
	}

	public function GetAll()
	{
		$results = $this->ExecuteQuery("SELECT * FROM USERS");

		$userResults = array();

		foreach ($results as $k)
		{
			$userResult = new User();

			$userResult->set_userID($k->userID);
			$userResult->set_username($k->username);
			$userResult->set_password($k->password);
			$userResult->set_name($k->name);
			$userResult->set_lastname($k->lastname);
			$userResult->set_role($k->role);
			$userResult->set_rank($k->rank);
			$userResult->set_sex($k->sex);
			$userResults[] = $userResult;
		}

		return $userResults;
	}

	public function AddOne($object)
	{

		$object->ValidateFields();

		$object->set_username(strtolower($object->get_username()));
		$object->set_password(hash('sha256', $object->get_password()));

		$query = 
		"INSERT INTO USERS 
		VALUES
		(
		NULL,'"
		.$object->get_username()."','"
		.$object->get_password()."','"
		.ucfirst($object->get_name())."','"
		.ucfirst($object->get_lastname())."','"
		.$object->get_role()."','"
		.$object->get_rank()."','"
		.$object->get_sex()
		."');";

		return $this->ExecuteQuery($query);
	}

	public function EditOne($object)
	{

		$object->ValidateFields();

		$object->set_username(strtolower($object->get_username()));

		$query= "SELECT password FROM USERS where userID =".$object->get_userID();

		$result = $this->ExecuteQuery($query);

		if($object->get_password() == $result[0]->password)
		{
			$query = 
			"UPDATE USERS SET 
			 username = '".$object->get_username()."', 
			 name ='".ucfirst($object->get_name())."', 
			 lastname ='".ucfirst($object->get_lastname())."', 
			 role ='".$object->get_role()."', 
			 rank ='".$object->get_rank()."', 
			 sex ='".$object->get_sex()."'
			 WHERE
			 userID = ".$object->get_userID().";";

			 return $this->ExecuteQuery($query);
		}
		else
		{
			$object->set_password(hash('sha256',$object->get_password()));
			$query = 
			"UPDATE USERS SET 
			 username = '".$object->get_username()."', 
			 password ='".$object->get_password()."', 
			 name ='".ucfirst($object->get_name())."', 
			 lastname ='".ucfirst($object->get_lastname())."', 
			 role ='".$object->get_role()."',
			 rank ='".$object->get_rank()."',  
			 sex ='".$object->get_sex()."'
			 WHERE
			 userID = ".$object->get_userID().";";

			 return $this->ExecuteQuery($query);
		}




	}

	public function DeleteOne($object)
	{
		$object->ValidateFields();
		$query =
			"DELETE FROM USERS
			 WHERE 
			 userID = ".$object->get_userID();
		return $this->ExecuteQuery($query);
	}

	public function GetOne($object)
	{

		$userResult = new User();
		$userResult->set_userID($object);

		$results = $this->ExecuteQuery("
			SELECT *
			FROM
			USERS
			WHERE
			userID =".$userResult->get_userID());

		if(count($results)>0)
		{
			$k = $results[0];

			$userResult = new User();
			$userResult->set_userID($k->userID);
			$userResult->set_username($k->username);
			$userResult->set_password($k->password);
			$userResult->set_name($k->name);
			$userResult->set_lastname($k->lastname);
			$userResult->set_role($k->role);
			$userResult->set_rank($k->rank);
			$userResult->set_sex($k->sex);

			return $userResult;
		}

			return false;
	}

	public function SearchUser($field, $search)
	{
		$results = $this->ExecuteQuery("
			SELECT *
			FROM
			USERS
			WHERE 
			".$field." 
			LIKE 
			'%".$search."%'");

		$userResults = array();

		foreach ($results as $k)
		{
			$userResult = new User();

			$userResult->set_userID($k->userID);
			$userResult->set_username($k->username);
			$userResult->set_password($k->password);
			$userResult->set_name($k->name);
			$userResult->set_lastname($k->lastname);
			$userResult->set_role($k->role);
			$userResult->set_rank($k->rank);
			$userResult->set_sex($k->sex);
			$userResults[] = $userResult;
		}

		if(empty($userResult))
		{
			return false;			
		} 

			return $userResults;
	}

	public function FindUser($username,$password)
	{
		$userTMP = new User();
		$userTMP->set_username($username);
		$userTMP->set_password($password);

		$userTMP->ValidateFields();

		$usernameMOD = strtolower($userTMP->get_username());
		$passwordMOD = hash('sha256',$userTMP->get_password());

		$results = $this->ExecuteQuery("
					SELECT *
					FROM USERS
					WHERE
					username='".$usernameMOD.
					"' AND 
					password='".$passwordMOD."'"
			);

		$userResult = new User();

		if(count($results)>0)
		{
			$k = $results[0];

			$userResult = new User();
			$userResult->set_userID($k->userID);
			$userResult->set_username($k->username);
			$userResult->set_password($k->password);
			$userResult->set_name($k->name);
			$userResult->set_lastname($k->lastname);
			$userResult->set_role($k->role);
			$userResult->set_rank($k->rank);
			$userResult->set_sex($k->sex);

			return $userResult;
		}
		else
		{
			return false;
		}
	}

		public function ManageRank()
	{
			$query =
			"UPDATE USERS
			 SET rank = NULL 
			 WHERE 
			 role <> 'professor'";

			return $this->ExecuteQuery($query);
	}
}
?>