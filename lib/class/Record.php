<?php

require_once 'InterfaceFieldValidator.php';

class Record implements JsonSerializable, FieldValidator
{
	protected $_recordID = 0;
	protected $_users_userID = 0;
	protected $_date = "";
	protected $_arrival = "";
	protected $_departure = "";
	protected $_author = "";
	protected $_lastEdit ="";


	function __construct() {}


	function get_recordID()
	{
		return $this->_recordID;
	}

	function get_users_userID()
	{
		return $this->_users_userID;
	}

	function get_date()
	{
		return $this->_date;
	}

	function get_arrival()
	{
		return $this->_arrival;
	}

	function get_departure()
	{
		return $this->_departure;
	}

	function get_author()
	{
		return $this->_author;
	}

	function get_lastEdit()
	{
		return $this->_lastEdit;
	}

	function set_recordID($_recordID)
	{
		$this->_recordID = $_recordID;
	}

	function set_users_userID($_users_userID)
	{
		$this->_users_userID = $_users_userID;
	}

	function set_date($_date)
	{
		$this->_date = $_date;
	}

	function set_arrival($_arrival)
	{
		$this->_arrival = $_arrival;
	}

	function set_departure($_departure)
	{
		$this->_departure = $_departure;
	}

	function set_author($_author)
	{
		$this->_author = $_author;
	}

	function set_lastEdit($_lastEdit)
	{
		$this->_lastEdit = $_lastEdit;
	}


		public function jsonSerialize()
	{
		return (object) get_object_vars($this);
	}

	public function jsonDeserialize($data)
	{
		$this->_recordID = $data->{'_recordID'};
		$this->_users_userID= $data->{'_users_userID'};
		$this->_date = $data->{'_date'};
		$this->_arrival = $data->{'_arrival'};
		$this->_departure = $data->{'_departure'};
		$this->_author = $data->{'_author'};
		$this->_lastEdit = $data->{'_lastEdit'};
	}

	public function ValidateFields()
	{
		$this->_recordID = formatInput($this->_recordID);
		$this->_users_userID= formatInput($this->_users_userID);
		$this->_date = formatInput($this->_date);
		$this->_arrival = formatInput($this->_arrival);
		$this->_departure = formatInput($this->_departure);
		$this->_author = formatInput($this->_author);
		$this->_lastEdit = formatInput($this->_lastEdit);
	}

}
?>