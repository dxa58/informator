<?php

require_once 'InterfaceFieldValidator.php';
require_once 'inputFormaterCustom.php';

class User implements JsonSerializable, FieldValidator
{
	protected $_userID = 0;
	protected $_username = "";
	protected $_password = "";
	protected $_name = "";
	protected $_lastname = "";
	protected $_role = "";
	protected $_rank = "";
	protected $_sex ="";


	function __construct() {}


	function get_userID()
	{
		return $this->_userID;
	}

	function get_username()
	{
		return $this->_username;
	}

	function get_password()
	{
		return $this->_password;
	}

	function get_name()
	{
		return $this->_name;
	}

	function get_lastname()
	{
		return $this->_lastname;
	}

	function get_role()
	{
		return $this->_role;
	}

		function get_rank()
	{
		return $this->_rank;
	}

	function get_sex()
	{
		return $this->_sex;
	}

	function set_userID($_userID)
	{
		$this->_userID = $_userID;
	}

	function set_username($_username)
	{
		$this->_username = $_username;
	}

	function set_password($_password)
	{
		$this->_password = $_password;
	}

	function set_name($_name)
	{
		$this->_name = $_name;
	}

	function set_lastname($_lastname)
	{
		$this->_lastname = $_lastname;
	}

	function set_role($_role)
	{
		$this->_role = $_role;
	}

	function set_rank($_rank)
	{
		$this->_rank = $_rank;
	}

	function set_sex($_sex)
	{
		$this->_sex = $_sex;
	}


	public function jsonSerialize()
	{
		return (object) get_object_vars($this);
	}

	public function jsonDeserialize($data)
	{
		$this->_userID = $data->{'_userID'};
		$this->_username= $data->{'_username'};
		$this->_password = $data->{'_password'};
		$this->_name = $data->{'_name'};
		$this->_lastname = $data->{'_lastname'};
		$this->_role = $data->{'_role'};
		$this->_rank = $data->{'_rank'};
		$this->_sex = $data->{'_sex'};
	}

	public function ValidateFields()
	{
		$this->_userID = formatInput($this->_userID);
		$this->_username= formatInput($this->_username);
		$this->_password = formatInput($this->_password);
		$this->_name = formatInput($this->_name);
		$this->_lastname = formatInput($this->_lastname);
		$this->_role = formatInput($this->_role);
		$this->_rank = formatInput($this->_rank);
		$this->_sex = formatInput($this->_sex);
	}
}
?>