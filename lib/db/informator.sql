-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 14, 2017 at 09:37 AM
-- Server version: 5.7.14-log
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `informator`
--
CREATE DATABASE IF NOT EXISTS `informator` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `informator`;

-- --------------------------------------------------------

--
-- Table structure for table `records`
--

CREATE TABLE `records` (
  `recordID` int(11) NOT NULL,
  `users_userID` int(11) NOT NULL,
  `date` date NOT NULL,
  `arrival` time NOT NULL,
  `departure` time DEFAULT NULL,
  `author` enum('user','administrator','automatic') NOT NULL,
  `lastEdit` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `records`
--

INSERT INTO `records` (`recordID`, `users_userID`, `date`, `arrival`, `departure`, `author`, `lastEdit`) VALUES
(15, 12, '2017-09-13', '12:00:00', '13:00:00', 'administrator', '2017-09-13 14:48:28'),
(17, 12, '2017-09-13', '12:23:00', '13:23:00', 'administrator', '2017-09-13 14:54:55'),
(18, 12, '2017-09-13', '13:37:00', '16:20:00', 'administrator', '2017-09-13 14:55:32'),
(19, 12, '2017-09-12', '12:12:00', '13:13:00', 'administrator', '2017-09-13 22:45:30'),
(21, 14, '2017-09-11', '11:23:00', '16:45:00', 'administrator', '2017-09-13 23:37:11'),
(22, 13, '2017-09-13', '11:11:00', '22:22:00', 'administrator', '2017-09-13 23:37:50'),
(23, 15, '2017-09-14', '11:11:00', '11:22:00', 'administrator', '2017-09-13 23:38:02');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `userID` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(256) NOT NULL,
  `name` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `role` enum('student','professor','administrator') NOT NULL,
  `rank` enum('Saradnik u nastavi','Asistent','Docent','Vanredni profesor','Redovni profesor') DEFAULT NULL,
  `sex` enum('male','female') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userID`, `username`, `password`, `name`, `lastname`, `role`, `rank`, `sex`) VALUES
(1, 'admin', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 'admin', 'admin', 'administrator', NULL, 'male'),
(3, 'student', '264c8c381bf16c982a4e59b0dd4c6f7808c51a05f64c35db42cc78a2a72875bb', 'Neki', 'Lik', 'student', NULL, 'male'),
(12, 'profesor', '48a8d9ca65d4ffed5d24d09cde13ef76320e7d4ebf468bcfcb5c4a17f87785a9', 'Petar', 'PetroviÄ‡', 'professor', 'Saradnik u nastavi', 'male'),
(13, 'profesor2', 'c59036fb2b020cac117abc9e4647f54bac565eddbb5aa209f9e78e5269e0ec42', 'Drugi', 'Profesor', 'professor', 'Vanredni profesor', 'male'),
(14, 'asistent', 'a58ab1e856dc0dc06854be342eaf6ede89a13399bd04e6bc5b96e8d73289d8d8', 'ÄorÄ‘e', 'Å Ä‡ekiÄ‡', 'professor', 'Asistent', 'male'),
(15, 'docent', '4c48f7dbc22711b189f59d114b93abd825816c962dc34da18d70dcefe6485cea', 'ÐœÐ¸ÑšÐ°', 'Ð‰ÑƒÐ±Ð¸ÑˆÐ¸Ñ›', 'professor', 'Docent', 'female'),
(17, 'nedolazi', '37c461e2599fc43f6e2479adb1500fe2c8f281254ec399752ff3d9cc83f675d7', 'ÄŒlan veÄ‡a', 'U inostranstvu', 'professor', 'Redovni profesor', 'male');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `records`
--
ALTER TABLE `records`
  ADD PRIMARY KEY (`recordID`),
  ADD KEY `users_userID` (`users_userID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userID`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `records`
--
ALTER TABLE `records`
  MODIFY `recordID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `userID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `records`
--
ALTER TABLE `records`
  ADD CONSTRAINT `records_ibfk_1` FOREIGN KEY (`users_userID`) REFERENCES `users` (`userID`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
