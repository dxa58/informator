<?php
session_start();
if(isset($_SESSION['role']))
{
	if($_SESSION['role']=='professor')
	{
		header('Location: professor.php');
	}
	if($_SESSION['role']=='administrator')
	{
		header('Location: administrator.php');
	}
	if($_SESSION['role']=='student')
	{
		header('Location: student.php');
	}
}
?>
<!DOCTYPE HTML>
 <head>
 <title>Informator</title>
 <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  <link rel="stylesheet" type="text/css" href="css/login.css">
  <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
</head> 


<div class="container">
    <div class="row">
        <div class="col-md-offset-5 col-md-3">
        	<div class="centered">
            <div class="form-login">
            <?php
            if(isset($_SESSION['pleaseLogin']))
            {
            	echo "<h4>Neautorizovan pristup</h4>";
            	session_unset();
            	session_destroy();
            }
            else
            if(isset($_SESSION['loginFail']))
            {
            	echo "<h4>Neuspešno logovanje</h4>";
            	session_unset();
            	session_destroy();
            }
            else
            if(isset($_SESSION['invalidUser']))
            {
            	echo "<h4>Nepostojeći user/pass</h4>";
            	session_unset();
            	session_destroy();
            }
            else
            	echo "<h4>Dobro došli</h4>";
            ?>
            <form action="login.php" method="POST">
            <input type="text" id="userName" name="username" class="form-control input-sm chat-input" placeholder="Korisničko ime" required="true" />
            <br>
            <input type="password" id="userPassword" name="password" class="form-control input-sm chat-input" placeholder="Lozinka" required="true" />
            <br>
            <div class="wrapper">
            <span class="group-btn">     
                <input type="submit" class="btn btn-primary btn-md" value="Prijavi se">
            </span>
            </div>
            </form>
            <br>
            <div class="wrapper">
            <a href="register.php">Kreiraj nalog</a>   
            </div>
            </div>
        	</div>
        </div>
    </div>
</div>
