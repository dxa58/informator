<?php
session_start();
if(isset($_SESSION['userID']))
{
	header('Location: profile.php');
}

require_once 'lib/utility/actualURL.php';

$curl = curl_init($actual_link."services/login");

if (!empty($_POST['username']) && !empty($_POST['password']))
{
	$urlPOST = $actual_link."/services/login";
	$curl_post_data = array(
		'username' => $_POST['username'],
		'password' => $_POST['password'],
	);
	$curl = curl_init($urlPOST);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
	$result = curl_exec($curl);
	$out = json_decode($result);
	if (count($out)>1)
	{
		if ($out[0]==true)
		{
			// create session

			$_SESSION['userID']=$out[1];
			$_SESSION['username']=$out[2];
			$_SESSION['password']=$out[3];
			$_SESSION['name']=$out[4];
			$_SESSION['lastname']=$out[5];
			$_SESSION['role']=$out[6];
			$_SESSION['rank']=$out[7];
			$_SESSION['sex']=$out[8];

			switch ($_SESSION['role'])
			{
				case 'student':
					header('Location: student.php');
					break;

				case 'professor':
					header('Location: professor.php');
					break;

				case 'administrator':
					header('Location: administrator.php');
					break;
			}
			
		}
		else
		{
			$_SESSION['loginFail'] = true;
			header('Location: index.php');
			
		}
	}
	else
	{
		 //obavestiti korisnika o neuspelom logovanju
		if (is_null($out))
		{
			$_SESSION['invalidUser'] = true;
			header('Location: index.php');
		} 
			
	}
}
?>