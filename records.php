<?php
session_start();
require_once 'lib/class/Record.php';
require_once 'lib/class/User.php';

if (!isset($_SESSION['username']))
{
	$_SESSION['pleaseLogin'] = true;
	header('Location: index.php');
}

if($_SESSION['role']=='professor')
{
	header('Location: professor.php');
}
if($_SESSION['role']=='student')
{
	header('Location: student.php');
}

require_once 'lib/utility/actualURL.php';

if(!empty($_POST))
{
	if($_POST['field'] == 'name')
	{
		$urlPOST = $actual_link."/services/searchuser";
		$curl_post_data = array(
			'field' => "CONCAT(name,' ',lastname)",
			'search' => $_POST['search']
		);
		$curl = curl_init($urlPOST);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$result = curl_exec($curl);
		$data = json_decode($result);
		$users = array();

		for ($i=0; $i<=count($data)-1;$i++)
		{
			$user = new User();
			$user->jsonDeserialize($data[$i]);

			array_push($users,$user);
		}

		$records = array();

		foreach ($users as $user)
		{
			$urlPOST = $actual_link."/services/searchrecords";
			$curl_post_data = array(
				'field' => 'users_userID',
				'search' => $user->get_userID()
			);
			$curl = curl_init($urlPOST);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
			$result = curl_exec($curl);
			$data = json_decode($result);

			for ($i=0; $i<=count($data)-1;$i++)
			{
				$record = new Record();
				$record->jsonDeserialize($data[$i]);

				array_push($records,$record);
			}
		}
	}
	else
	{
		$urlPOST = $actual_link."/services/searchrecords";
		$curl_post_data = array(
			'field' => $_POST['field'],
			'search' => $_POST['search']
		);
		$curl = curl_init($urlPOST);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$result = curl_exec($curl);
		$data = json_decode($result);
		$records = array();
		for ($i=0; $i<=count($data)-1;$i++)
		{
			$record = new Record();
			$record->jsonDeserialize($data[$i]);

			array_push($records,$record);
		}
	}	
}
else
{
	$targetURL = $actual_link."/services/records";


	$curl = curl_init($targetURL);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	$response = curl_exec($curl);
	$data = json_decode($response);
	$records = array();
	for ($i=0; $i<=count($data)-1;$i++)
	{
		$record = new Record();
		$record->jsonDeserialize($data[$i]);

		array_push($records,$record);
	}
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Evidencija zaposlenih</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen, projection, tv" />
<link rel="stylesheet" href="css/style-print.css" type="text/css" media="print" />
</head>
<body>
<div id="wrapper">
</div>
  <hr class="noscreen" />
  <div class="content">
    <div class="column-left">
      <h3>ADMIN MENI</h3>
      <a href="#skip-menu" class="hidden">Skip menu</a>
      <ul class="menu">
      	<nav>
        <li><a href="<?php echo $actual_link."/administrator.php";?>">Naslovna</a></li>
        <li><a href="<?php echo $actual_link."/records.php";?>" class="active" >Evidencije</a></li>
        <li><a href="<?php echo $actual_link."/users.php";?>" >Korisnici</a></li>
        <li><a href="<?php echo $actual_link."/regularity.php";?>">Redovnost dolazaka</a></li>
        <li><a href="<?php echo $actual_link."/consecutive-absence.php";?>">Uzastopno odsustvo</a></li>
        <li><a href="<?php echo $actual_link."/changepassword.php";?>">Promena lozinke</a></li>
        <li><a href="<?php echo $actual_link."/logout.php";?>" class="last">Odjava</a></li>		
      	</nav>
      </ul>
    </div>
    <div id="skip-menu"></div>
    <div class="column-right">
      <div class="box">
        <div class="box-top"></div>
        <div class="box-in">
         <h2>Evidencije</h2>
         <form action="records.php" method="post">
         	<input type="text" name="search" <?php if(!empty($_POST)) echo "value=\"".$_POST['search']."\" "?> required>
         	<select name="field">
         		<option value="name" <?php if(!empty($_POST) && $_POST['field'] == "name") echo "selected";?>>Ime i Prezime</option>
         		<option value="date" <?php if(!empty($_POST) && $_POST['field'] == "date") echo "selected";?>>Datum</option>
         		<option value="recordID" <?php if(!empty($_POST) && $_POST['field'] == "recordID") echo "selected";?>>ID Evidencije</option>
         		<option value="author" <?php if(!empty($_POST) && $_POST['field'] == "author") echo "selected";?>>Autor</option>
         	</select>
         	<input type="submit" name="submit" value="Pretraži">
         </form><br>
         		<form action="record.php">
			<input type="submit" value="Nova evidencija"/>
		</form><br>
		<?php if(count($records) !=1)
		{
			echo count($records)." rezultata";
		}
		else
		{
			echo count($records)." rezultat";
		}?>
          <table class="table-student">
				<tr>
					<th>
					ID
					</th>
					<th>
					Kor.
					</th>
					<th>
					Zvanje
					</th>
					<th>
					Ime
					</th>
					<th>
					Prezime
					</th>
					<th>
					Datum
					</th>
					<th>
					Dolazak
					</th>
					<th>
					Odlazak
					</th>
					<th>
					Autor
					</th>
					<th>
					Izmena
					</th>
					<th>
					Izmeni
					</th>
					<th>
					Obrisi
					</th>
				</tr>
			<?php foreach($records as $r)
			{ 
					$urlPOST = $actual_link."/services/searchuser";
					$curl_post_data = array(
						'field' => "userID",
						'search' => $r->get_users_userID()
					);
					$curl = curl_init($urlPOST);
					curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($curl, CURLOPT_POST, true);
					curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
					$result = curl_exec($curl);
					$dataUser = json_decode($result);
					$user = new User();
					$user->jsonDeserialize($dataUser[0]);

				?>
				<tr>
					<td>
					<?php echo $r->get_recordID(); ?>
					</td>
					<td class="tdright">
					<?php echo $r->get_users_userID(); ?>
					</td>
					<td class="tdright">
					<?php echo $user->get_rank(); ?>
					</td>
					<td class="tdright">
					<?php echo $user->get_name(); ?>
					</td>
					<td class="tdright">
					<?php echo $user->get_lastname(); ?>
					</td>
					<td class="tdright">
					<?php echo $r->get_date(); ?>
					</td>
					<td class="tdright">
					<?php echo $r->get_arrival(); ?>
					</td>
					<td class="tdright">
					<?php echo $r->get_departure(); ?>
					</td>
					<td class="tdright">
					<?php echo $r->get_author(); ?>
					</td>
					<td class="tdright">
					<?php echo $r->get_lastEdit(); ?>
					</td>
					<td class="tdright">
                        <form name="edit<?php echo $r->get_recordID(); ?>" method="GET" action="record.php">
							<input type="hidden" name="recordID" value="<?php echo $r->get_recordID(); ?>"/>
							<input type="Button" value="Izmeni" onclick="document.edit<?php echo $r->get_recordID(); ?>.submit()"/>
						</form>
					</td>
					<td class="tdright">
						<form name="delete<?php echo $r->get_recordID(); ?>" method="POST" action="delete.php">
							<input type="hidden" name="recordID" id="id" value="<?php echo $r->get_recordID(); ?>"/>
							<input type="Button" value="Obriši" onclick="document.delete<?php echo $r->get_recordID(); ?>.submit()"/>
						</form>
					</td>
				</tr>
			<?php }; $_SESSION['data'] = $data; ?>
			</table> 
			<br>
			<form action="print.php">
    			<input type="submit" value="Štampa" />
			</form>
        </div>
      </div>
    </div>
    <div class="cleaner">&nbsp;</div>
  </div>
</body>
</html>
