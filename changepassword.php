<?php
require_once 'lib/DAL/UserDAL.php';
require_once 'lib/class/User.php';
session_start();

if (!isset($_SESSION['username']))
{
	$_SESSION['pleaseLogin'] = true;
	header('Location: index.php');
}

if($_SESSION['role']=='professor')
{
	$main = 'professor.php';
}
if($_SESSION['role']=='student')
{
	$main = 'student.php';
}
if($_SESSION['role']=='administrator')
{
	$main = 'administrator.php';
}

require_once 'lib/utility/actualURL.php';

if(!empty($_POST))
{
	$current = hash('sha256',$_POST['current']);
	if($_SESSION['password'] == $current)
	{
		if($_POST['current'] !=$_POST['new'])
		{
			if($_POST['new'] ==$_POST['repeat'])
			{

				$urlPOST = $actual_link."/services/edituser/";
				$curl_post_data = array(
				'userID' => $_SESSION['userID'],
				'username' => $_SESSION['username'],
				'password' => $_POST['repeat'],
				'name' => $_SESSION['name'],
				'lastname' => $_SESSION['lastname'],
				'role' => $_SESSION['role'],
				'sex' => $_SESSION['sex']
				);
				$curl = curl_init($urlPOST);
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($curl, CURLOPT_POST, true);
				curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
				$response = curl_exec($curl);
				if ($response) 
					{
						$message = 'Lozinka uspešno promenjena!';

						$targetURL = $actual_link."/services/user?userID=".$_SESSION['userID'];
						$curl = curl_init($targetURL);
						curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
						$response = curl_exec($curl);
						$data = json_decode($response);
						$user = new User();
						$user->jsonDeserialize($data[0]);
						$_SESSION['password'] = $user->get_password();
					}
			}
			else
				$message = 'Uneta nova lozinka i ponovljena nova lozinka nisu iste!';
		}
		else
			$message = 'Lozinka mora biti različita od trenutne!';
	}
	else
		$message = 'Uneta trenutna lozinka nije validna!';

}

?>
 <!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Evidencija zaposlenih</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen, projection, tv" />
<link rel="stylesheet" href="css/style-print.css" type="text/css" media="print" />
</head>
<body>
<div id="wrapper">
</div>
  <hr class="noscreen" />
  <div class="content">
    <div class="column-left">
      <h3>MENI</h3>
      <a href="#skip-menu" class="hidden">Skip menu</a>
      <ul class="menu">
      	<nav>
        <li><a href="<?php echo $actual_link."/".$main;?>" >Naslovna</a></li>
        <li><a href="<?php echo $actual_link."/changepassword.php";?>"class="active">Promena lozinke</a></li>
        <li><a href="<?php echo $actual_link."/logout.php";?>" class="last">Odjava</a></li>
        </nav>
      </ul>
    </div>
    <div id="skip-menu"></div>
    <div class="column-right">
      <div class="box">
        <div class="box-top"></div>
        <div class="box-in">
          <h2>Promena lozinke</h2>
          <br />
        <?php 
        if(isset($message))
        {
        	echo "<ul>
        	<li>".$message."</li>
        </ul><br>";
        } ?>
           	<form action="/changepassword.php" method="POST">
			  <input type="password" name="current" placeholder="Trenutna lozinka" required="true"><br><br>
			  <input type="password" name="new" placeholder="Nova lozinka" required="true"><br><br>
			  <input type="password" name="repeat" placeholder="Nova lozinka ponovo" required="true"><br><br>
			  <input type="submit" value="Promeni">
			</form> 
        </div>
      </div>
    </div>
    <div class="cleaner">&nbsp;</div>
  </div>
</div>
</body>
</html>