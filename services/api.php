<?php
 	require_once "Rest.php" ;
    require_once '../lib/DAL/UserDAL.php';
    require_once '../lib/DAL/RecordDAL.php';
    require_once '../lib/DAL/DALQueryResults.php';
	
	class API extends REST 
        {

            public function __construct()
            {
                parent::__construct();
            }

            public function processApi()
            {
                $func = strtolower(trim(str_replace("/", "", $_REQUEST['x'])));
                if ((int) method_exists($this, $func) > 0)
                    $this->$func();
                else
                    $this->response('false', 404);
            }

            public function json($data)
            {
                if (is_array($data))
                {
                    return json_encode($data);
                }
            }

//==================================================================================
            //USERS

            private function users()
            {

                if ($this->get_request_method() != "GET")
                    $this->response('false', 406);

                $UserDAL = new UserDAL();
                $users = $UserDAL->GetAll();
                $this->response($this->json($users), 200);
            }
            
            private function searchuser()
            {

                if ($this->get_request_method() != "POST")
                    $this->response('false', 406);
                
                $field = (string) $this->_request['field'];
                $search = (string) $this->_request['search'];

                               
                $UserDAL = new UserDAL();
                $users = $UserDAL->SearchUser($field,$search);
                $this->response($this->json($users), 200);
            }

            private function user()
            {

                if ($this->get_request_method() != "GET")
                {
                    $this->response('false', 406);
                }

                $userID = (string) $this->_request['userID'];
                if ($userID > 0)
                {
                    $UserDAL = new UserDAL();
                    $user = $UserDAL->GetOne($userID);
                    $result = array();
                    array_push($result, $user);
                    $this->response($this->json($result), 200);
                }
                $this->response('false', 204);
            }

            private function adduser()
            {
                if ($this->get_request_method() != "POST")
                {
                    $this->response('false', 406);
                }

                $user = new User();
                $user->set_username((string) $this->_request['username']);
                $user->set_password((string) $this->_request['password']);
                $user->set_name((string) $this->_request['name']);
                $user->set_lastname((string) $this->_request['lastname']);
                $user->set_role((string) $this->_request['role']);
                $user->set_rank((string) $this->_request['rank']);
                $user->set_sex((string) $this->_request['sex']);

                $UserDAL = new UserDAL();
                $succ = $UserDAL->AddOne($user);
                if (!$succ) $this->response('false', 204);
                else $this->response('true', 200);
  
            }

            private function edituser()
            {
                if ($this->get_request_method() != "POST")
                {
                    $this->response('false', 406);
                }

                $user = new User();
                $user->set_userID((string) $this->_request['userID']);
                $user->set_username((string) $this->_request['username']);
                $user->set_password((string) $this->_request['password']);
                $user->set_name((string) $this->_request['name']);
                $user->set_lastname((string) $this->_request['lastname']);
                $user->set_role((string) $this->_request['role']);
                $user->set_rank((string) $this->_request['rank']);
                $user->set_sex((string) $this->_request['sex']);

                $UserDAL = new UserDAL();
                $succ = $UserDAL->EditOne($user);
                if (!$succ) $this->response('false', 204);
                else $this->response('true', 200);
            }

            private function deleteuser()
            {
                if ($this->get_request_method() != "POST")
                {
                    $this->response('false', 406);
                }

                $user = new User();
                $user->set_userID((string) $this->_request['userID']);
                $UserDAL = new UserDAL();
                $succ = $UserDAL->DeleteOne($user);
                if (!$succ) $this->response('false', 204);
                else $this->response('true', 200);
            }


            private function login()
            {
                if ($this->get_request_method() != "POST")
                {
                    $this->response('false', 406);
                }
                $username = (string) $this->_request['username'];
                $password = (string) $this->_request['password'];
                $userDAL = new UserDAL();
                $user = $userDAL->FindUser($username, $password);
                $result = array();
                if ($user->get_userID()>0)
                {
                array_push($result, true);
                array_push($result, $user->get_userID());
                array_push($result, $user->get_username());
                array_push($result, $user->get_password());
                array_push($result, $user->get_name());
                array_push($result, $user->get_lastname());
                array_push($result, $user->get_role());
                array_push($result, $user->get_rank());
                array_push($result, $user->get_sex());
                }
                else array_push($result, false);
                $this->response($this->json($result), 200);
            }


            private function managerank()
            {

                if ($this->get_request_method() != "GET")
                {
                    $this->response('false', 406);
                }

                    $UserDAL = new UserDAL();
                    $user = $UserDAL->ManageRank();

                    $this->response($this->json($user), 200);
            }

//==================================================================================
            //RECORDS

            private function records()
            {

                if ($this->get_request_method() != "GET")
                    $this->response('false', 406);

                $recordDAL = new RecordDAL();
                $records = $recordDAL->GetAll();
                $this->response($this->json($records), 200);
            }
            
            private function searchrecords()
            {

                if ($this->get_request_method() != "POST")
                    $this->response('false', 406);
                
                $field = (string) $this->_request['field'];
                $search = (string) $this->_request['search'];

                               
                $recordDAL = new RecordDAL();
                $records = $recordDAL->SearchRecord($field,$search);
                $this->response($this->json($records), 200);
            }

            private function allpresent()
            {

                if ($this->get_request_method() != "GET")
                    $this->response('false', 406);

                $recordDAL = new RecordDAL();
                $records = $recordDAL->GetAllPresent();
                $this->response($this->json($records), 200);
            }

            private function record()
            {

                if ($this->get_request_method() != "GET")
                {
                    $this->response('false', 406);
                }

                $recordID = (string) $this->_request['recordID'];
                if ($recordID > 0)
                {
                    $recordDAL = new RecordDAL();
                    $record = $recordDAL->GetOne($recordID);
                    $result = array();
                    array_push($result, $record);
                    $this->response($this->json($result), 200);
                }
                $this->response('false', 204);
            }

            private function addrecord()
            {
                if ($this->get_request_method() != "POST")
                {
                    $this->response('false', 406);
                }

                $record = new Record();
                $record->set_users_userID((string) $this->_request['users_userID']);
                $record->set_date((string) $this->_request['date']);
                $record->set_arrival((string) $this->_request['arrival']);
                $record->set_departure((string) $this->_request['departure']);
                $record->set_author((string) $this->_request['author']);

                $recordDAL = new RecordDAL();
                $succ = $recordDAL->AddOne($record);
                if (!$succ) $this->response('false', 204);
                else $this->response('true', 200);
  
            }

            private function editrecord()
            {
                if ($this->get_request_method() != "POST")
                {
                    $this->response('false', 406);
                }

                $record = new Record();
                $record->set_recordID((string) $this->_request['recordID']);
                $record->set_users_userID((string) $this->_request['users_userID']);
                $record->set_date((string) $this->_request['date']);
                $record->set_arrival((string) $this->_request['arrival']);
                $record->set_departure((string) $this->_request['departure']);

                $recordDAL = new RecordDAL();
                $succ = $recordDAL->EditOne($record);
                if (!$succ) $this->response('false', 204);
                else $this->response('true', 200);
            }

            private function deleterecord()
            {
                if ($this->get_request_method() != "POST")
                {
                    $this->response('false', 406);
                }

                $record = new Record();
                $record->set_recordID((string) $this->_request['recordID']);
                $recordDAL = new RecordDAL();
                $succ = $recordDAL->DeleteOne($record);
                if (!$succ) $this->response('false', 204);
                else $this->response('true', 200);
            }

            private function arrival()
            {
                if ($this->get_request_method() != "POST")
                {
                    $this->response('false', 406);
                }

                $record = new Record();
                $record->set_users_userID((string) $this->_request['users_userID']);
                $record->set_author((string) $this->_request['author']);

                $recordDAL = new RecordDAL();
                $succ = $recordDAL->AddArrival($record);
                if (!$succ) $this->response('false', 204);
                else $this->response('true', 200);
  
            }

            private function departure()
            {
                if ($this->get_request_method() != "POST")
                {
                    $this->response('false', 406);
                }

                $record = new Record();
                $record->set_users_userID((string) $this->_request['users_userID']);

                $recordDAL = new RecordDAL();
                $succ = $recordDAL->AddDeparture($record);
                if (!$succ) $this->response('false', 204);
                else $this->response('true', 200);
  
            }

            private function present()
            {
                if ($this->get_request_method() != "POST")
                {
                    $this->response('false', 406);
                }

                $user = new User();
                $user->set_userID((string) $this->_request['userID']);

                $recordDAL = new RecordDAL();
                $succ = $recordDAL->Present($user);
                if (!$succ) $this->response('false', 204);
                else $this->response('true', 200);
  
            }


            private function regularity()
            {
                if ($this->get_request_method() != "POST")
                {
                    $this->response('false', 406);
                }

                $recordDAL = new RecordDAL();
                $result = 
                $recordDAL->Regularity($this->_request['dateStart'],$this->_request['dateEnd'],$this->_request['userID']);
                
                $this->response(json_encode($result), 200);

            }

            private function elapsed()
            {
                if ($this->get_request_method() != "POST")
                {
                    $this->response('false', 406);
                }

                $recordDAL = new RecordDAL();
                $result = 
                $recordDAL->Elapsed($this->_request['userID']);
                
                $this->response(json_encode($result), 200);

            }

        }
	
	
	$api = new API;
	$api->processApi();
?>