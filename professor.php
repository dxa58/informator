<?php
session_start();

if (!isset($_SESSION['username']))
{
	$_SESSION['pleaseLogin'] = true;
	header('Location: index.php');
}

if($_SESSION['role']=='student')
{
	header('Location: student.php');
}
if($_SESSION['role']=='administrator')
{
	header('Location: administrator.php');
}

require_once 'lib/utility/actualURL.php';

$urlPOST = $actual_link."/services/present/";

        $curl_post_data = array(
        'userID' => $_SESSION['userID']
        );
        $curl = curl_init($urlPOST);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
        $present = curl_exec($curl);

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Evidencija zaposlenih</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen, projection, tv" />
<link rel="stylesheet" href="css/style-print.css" type="text/css" media="print" />
</head>
<body>
<div id="wrapper">
<!--   <div class="title">  
	<h1>Evidencija zaposlenih</h1>
  </div> -->
</div>
  <hr class="noscreen" />
  <div class="content">
    <div class="column-left">
      <h3>MENI</h3>
      <a href="#skip-menu" class="hidden">Skip menu</a>
      <ul class="menu">
        <li><a href="<?php echo $actual_link."/professor.php";?>" class="active">Naslovna</a></li>
        <li><a href="<?php echo $actual_link."/myrecords.php";?>">Moje evidencije</a></li>
        <li><a href="<?php echo $actual_link."/changepassword.php";?>">Promena lozinke</a></li>
        <li><a href="<?php echo $actual_link."/logout.php";?>" class="last">Odjava</a></li>
      </ul>
    </div>
    <div id="skip-menu"></div>
    <div class="column-right">
      <div class="box">
        <div class="box-top"></div>
        <div class="box-in">
          <h2>Dobro <?php 
          		if($_SESSION['sex'] == 'male')
          			echo "došao, ";
          		else
          			echo "došla, ";
          		echo $_SESSION['name']." ".$_SESSION['lastname'];
          		?></h2>
          <hr>
          <br />
        <form action="arrival.php" method="post" accept-charset="utf-8">
          <input type="text" name="userID" value="<?php echo $_SESSION['userID'];?>" hidden>
        	<input type="submit" name="arrival" value="Dolazak" <?php if($present) echo "disabled";?>>	
        </form>
        <br>
		    <form action="departure.php" method="post" accept-charset="utf-8">
          <input type="text" name="userID" value="<?php echo $_SESSION['userID'];?>" hidden>
        	<input type="submit" name="departure" value="Odlazak" <?php if(!$present) echo "disabled";?>>	
        </form>
        </div>
      </div>
    </div>
    <div class="cleaner">&nbsp;</div>
  </div>
</body>
</html>