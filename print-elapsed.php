<?php
session_start();
require_once 'lib/class/Record.php';
require_once 'lib/class/User.php';

if (!isset($_SESSION['username']))
{
	$_SESSION['pleaseLogin'] = true;
	header('Location: index.php');
}

if($_SESSION['role']=='professor')
{
	header('Location: professor.php');
}
if($_SESSION['role']=='student')
{
	header('Location: student.php');
}

require_once 'lib/utility/actualURL.php';

$tableData = $_SESSION['tableData'];
?>
<!DOCTYPE HTML>
 <head>
 <title>Evidencija zaposlenih</title>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="css/style-printing.css" media="screen">
</head>
<body>
<br>
<table class="no-spacing" style="width:100%; padding:0" align="center" cellspacing="0" cellpadding="0" border="0" style="border-spacing: 0;">
<meta charset="UTF-8">
<tr>
<td style="width:10%;">
</td>
<td align="center" valign="middle"> 
<b><font face="Times New Roman" color="darkblue" size="5px">Tehnički fakultet "Mihajlo Pupin", Zrenjanin</font></b> </br>
</br>
</br>
</td>
<td style="width:10%;">
</td>
</tr>
<tr>
<td style="width:10%;">
</td>
<td align=center>
&nbsp;
</td>
<td style="width:10%;">
</td>
</tr>
<tr style="padding:0px;">
<td style="width:10%;">
</td>
<td align="center" valign="middle" style="width:80%; padding:0" > 
<table style="width:100%; padding:0" align="center" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">
<tr>
<td style="width:1%;">
</td>
<td style="width:80%;padding:0" cellspacing="0" cellpadding="0" border="0" valign="top">
<meta charset="UTF-8">
<table style="width:100%;style="width:100%; padding:0" align="center" cellspacing="0" cellpadding="0" border="0"  bgcolor="#D8E7F4">
<tr>
<td style="width:5%;">
</td>
<td align=center>
<font face="Times New Roman" color="darkblue" size="4px">
<b>Uzastopno odsustvo zaposlenih na dan <?php 
$today = getdate();
echo $today['mday'].".".$today['mon'].".".$today['year'];
?></br> </font>
</td>
<td style="width:5%;">
</td>
</tr>
<tr>
<td style="width:5%;">
</td>
<td align="left">
<br/>
<font face="Times New Roman" color="darkblue" size="4px">
<?php


				echo "<table style=\"width:98%; padding:0\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" border=\"1\"  bgcolor=\"#D8E7F4\">";
				echo "<tr>";
				echo "<td style=\"width:12%;\"align=center>";
				echo "<b><font face=\"Trebuchet MS\" color:#3F4534 size=\"3px\"><pre> Ime </pre></font><br/>"; 
				echo "</td>";
				echo "<td style=\"width:12%;\"align=center>";
				echo "<b><font face=\"Trebuchet MS\" color:#3F4534 size=\"3px\"><pre> Prezime </pre></font><br/>";
				echo "</td>";
				echo "<td style=\"width:12%;\"align=center>";
				echo "<b><font face=\"Trebuchet MS\" color:#3F4534 size=\"3px\"><pre> Zvanje </pre></font><br/>";
				echo "</td>";
				echo "<td style=\"width:12%;\"align=center>";
				echo "<b><font face=\"Trebuchet MS\" color:#3F4534 size=\"3px\"><pre> Poslednji dolazak </pre></font><br/>";
				echo "</td>";
				echo "<td style=\"width:12%;\"align=center>";
				echo "<b><font face=\"Trebuchet MS\" color:#3F4534 size=\"3px\"><pre> Broj radnih dana nedolazaka na posao </pre></font><br/>";
				echo "</td>";
				echo "</tr>";
			
			foreach ($tableData as $row) 
			{


					$user = new User();
					$user->jsonDeserialize($row[0]);

				
					echo "<tr>";
					echo "<td align=center>";
					echo "<font face=\"Trebuchet MS\" color:#3F4534 size=\"2px\">".$user->get_name()."</font><br/>";
					echo "</td>";
					echo "<td align=center>";
					echo "<font face=\"Trebuchet MS\" color:#3F4534 size=\"2px\">".$user->get_lastname()."</font><br/>";
					echo "</td>";
					echo "<td align=center>";
					echo "<font face=\"Trebuchet MS\" color:#3F4534 size=\"2px\">".$user->get_rank()."</font><br/>";
					echo "</td>";
					echo "<td align=center>";
					echo "<font face=\"Trebuchet MS\" color:#3F4534 size=\"2px\">".$row[1]->Last."</font><br/>";
					echo "</td>";
					echo "<td align=center>";
					echo "<font face=\"Trebuchet MS\" color:#3F4534 size=\"2px\">".$row[1]->Elapsed."</font><br/>";
					echo "</td>";
					echo "</tr>";

			}
				echo "</table>";
				echo "<br/>";
				echo "<br/>";
  
?>
</td>
<td style="width:5%;">
</td>
</tr>
</table>    
</td>
<td style="width:1%;">
</td>
</tr>
</table>
</td>
<td style="width:10%;">
</td>
</table>
</body>
</html>