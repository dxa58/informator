<?php
session_start();
require_once 'lib/class/Record.php';
require_once 'lib/class/User.php';


if (!isset($_SESSION['username']))
{
	$_SESSION['pleaseLogin'] = true;
	header('Location: index.php');
}

if($_SESSION['role']=='professor')
{
	header('Location: professor.php');
}
if($_SESSION['role']=='student')
{
	header('Location: student.php');
}

require_once 'lib/utility/actualURL.php';

if(!empty($_POST))
{

	  	$urlPOST = $actual_link."/services/searchuser";
	$curl_post_data = array(
		'field' => 'role',
		'search' => 'professor'
	);
	$curl = curl_init($urlPOST);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
	$result = curl_exec($curl);
	$data = json_decode($result);
	$users = array();
	for ($i=0; $i<=count($data)-1;$i++)
	{
		$user = new User();
		$user->jsonDeserialize($data[$i]);

		array_push($users,$user);
	}
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Evidencija zaposlenih</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen, projection, tv" />
<link rel="stylesheet" href="css/style-print.css" type="text/css" media="print" />

<script type="text/javascript" src="http://code.jquery.com/jquery-2.1.4.min.js"></script> 
<script src="//cdn.jsdelivr.net/webshim/1.14.5/polyfiller.js"></script>
<script>
webshims.setOptions('forms-ext', {types: 'date'});
webshims.polyfill('forms forms-ext');
$.webshims.formcfg = {
en: {
    dFormat: '-',
    dateSigns: '-',
    patterns: {
        d: "yy-mm-dd"
    }
}
};
</script>
</head>
<body>
<div id="wrapper">
</div>
  <hr class="noscreen" />
  <div class="content">
    <div class="column-left">
      <h3>ADMIN MENI</h3>
      <a href="#skip-menu" class="hidden">Skip menu</a>
      <ul class="menu">
        <li><a href="<?php echo $actual_link."/administrator.php";?>">Naslovna</a></li>
        <li><a href="<?php echo $actual_link."/records.php";?>">Evidencije</a></li>
        <li><a href="<?php echo $actual_link."/users.php";?>">Korisnici</a></li>
        <li><a href="<?php echo $actual_link."/regularity.php";?>" class="active">Redovnost dolazaka</a></li>
        <li><a href="<?php echo $actual_link."/consecutive-absence.php";?>">Uzastopno odsustvo</a></li>
        <li><a href="<?php echo $actual_link."/changepassword.php";?>">Promena lozinke</a></li>
        <li><a href="<?php echo $actual_link."/logout.php";?>" class="last">Odjava</a></li>
      </ul>
    </div>
    <div id="skip-menu"></div>
    <div class="column-right">
      <div class="box">
        <div class="box-top"></div>
        <div class="box-in">
          	<h2>Redovnost dolazaka</h2>
          	<table>
          	<form method="post" action="regularity.php">
          	<input type="text" name="userID" value="12"  hidden />
	 		<tr>
	 		<td>Početak intervala</td>
	 		<td>Kraj intervala</td>
	 		</tr>
	 		<tr>
			<td><input type="date" name="dateStart"  <?php if(!empty($_POST)) echo "value=\"".$_POST['dateStart']."\" "?> required /></td>
  			<td><input type="date" name="dateEnd"  <?php if(!empty($_POST)) echo "value=\"".$_POST['dateEnd']."\" "?> required /></td>
			</tr>
		  	</table>
			<input type="submit" value="Prikaži">
		  </form>
		  <br><br>
		  <?php 
		  if(!empty($_POST))
		  {
		  	$_SESSION['dateStart'] = $_POST['dateStart'];
		  	$_SESSION['dateEnd'] = $_POST['dateEnd'];
		  	?>
		  	<hr>
		  	<br>
		  	<table>
		  	<th>Ime</th>
		  	<th>Prezime</th>
		  	<th>Zvanje</th>
		  	<th>Broj dolazaka</th>
		  	<th>Poslednji dolazak</th>		  	
		  	<?php
		  		$tableData = array();
			  foreach($users as $user)
			  {
	  			  	$urlPOST = $actual_link."/services/regularity";
					$curl_post_data = array(
						'dateStart' => $_POST['dateStart'],
						'dateEnd' => $_POST['dateEnd'],
						'userID' => $user->get_userID()
					);
					$curl = curl_init($urlPOST);
					curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($curl, CURLOPT_POST, true);
					curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
					$result = curl_exec($curl);
					$data = json_decode($result);

					$row = array($user->jsonSerialize(),$data);
					array_push($tableData,$row);

					?>
					<tr>
					<td class="tdright"><?php echo $user->get_name();?></td>
					<td class="tdright"><?php echo $user->get_lastname()?></td>
					<td class="tdright"><?php echo $user->get_rank()?></td>
					<td class="tdright"><?php echo $data->Total;?></td>
					<td class="tdright"><?php echo $data->Last;?></td>
					</tr>
				<?php
			  }
			  ?>
			  </table>
			  <?php
			  $_SESSION['tableData'] = $tableData;
			  ?>
		  	<br>
			<form action="print-regularity.php">
    			<input type="submit" value="Štampa" />
			</form>
			  <?php
		  }		  
		  ?>
        </div>
      </div>
    </div>
    <div class="cleaner">&nbsp;</div>
  </div>
</body>
</html>