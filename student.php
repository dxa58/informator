<?php
require_once 'lib/class/Record.php';
require_once 'lib/class/User.php';
session_start();

if (!isset($_SESSION['username']))
{
	$_SESSION['pleaseLogin'] = true;
	header('Location: index.php');
}

if($_SESSION['role']=='professor')
{
	header('Location: professor.php');
}
if($_SESSION['role']=='administrator')
{
	header('Location: administrator.php');
}

require_once 'lib/utility/actualURL.php';

$targetURL = $actual_link."/services/allpresent";

$curl = curl_init($targetURL);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
$response = curl_exec($curl);
$data = json_decode($response);
$mixed = array();
for ($i=0; $i<=count($data)-1;$i++)
{
	$record = new Record();
	$record->jsonDeserialize($data[$i][0]);

	$user = new User();
	$user->jsonDeserialize($data[$i][1]);

	$mix = array(
		0 => $record,
		1 => $user);
	array_push($mixed,$mix);
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Evidencija zaposlenih</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen, projection, tv" />
<link rel="stylesheet" href="css/style-print.css" type="text/css" media="print" />
</head>
<body>
<div id="wrapper">
<!--   <div class="title">  
	<h1>Evidencija zaposlenih</h1>
  </div> -->
</div>
  <hr class="noscreen" />
  <div class="content">
    <div class="column-left">
      <h3>MENI</h3>
      <a href="#skip-menu" class="hidden">Skip menu</a>
      <ul class="menu">
        <li><a href="<?php echo $actual_link."/student.php";?>" class="active">Naslovna</a></li>
        <li><a href="<?php echo $actual_link."/changepassword.php";?>">Promena lozinke</a></li>
        <li><a href="<?php echo $actual_link."/logout.php";?>" class="last">Odjava</a></li>
      </ul>
    </div>
    <div id="skip-menu"></div>
    <div class="column-right">
      <div class="box">
        <div class="box-top"></div>
        <div class="box-in">
          <h2>Spisak trenutno prisutnog nastavnog osoblja</h2>
          <table class="table-student">
				<tr>
					<th>
					Zvanje
					</th>
					<th>
					Ime
					</th>
					<th>
					Prezime
					</th>
					<th>
					Vreme dolaska
					</th>
				</tr>
			<?php foreach($mixed as $r): ?>
				<tr>
					<td>
					<?php echo $r[1]->get_rank(); ?>
					</td>
					<td>
					<?php echo $r[1]->get_name(); ?>
					</td>
					<td>
					<?php echo $r[1]->get_lastname(); ?>
					</td>
					<td class="tdright">
					<?php echo substr($r[0]->get_arrival(), 0, -3); ?>
					</td>
				</tr>
			<?php endforeach; ?>
			</table> 
          <br />
        </div>
      </div>
    </div>
    <div class="cleaner">&nbsp;</div>
  </div>
</body>
</html>