<?php
session_start();
require_once 'lib/class/User.php';

if (!isset($_SESSION['username']))
{
	$_SESSION['pleaseLogin'] = true;
	header('Location: index.php');
}

if($_SESSION['role']=='professor')
{
	header('Location: professor.php');
}
if($_SESSION['role']=='student')
{
	header('Location: student.php');
}

require_once 'lib/utility/actualURL.php';


  	$urlPOST = $actual_link."/services/searchuser";
	$curl_post_data = array(
		'field' => 'role',
		'search' => 'professor'
	);
	$curl = curl_init($urlPOST);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
	$result = curl_exec($curl);
	$data = json_decode($result);
	$users = array();
	for ($i=0; $i<=count($data)-1;$i++)
	{
		$user = new User();
		$user->jsonDeserialize($data[$i]);

		array_push($users,$user);
	}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Evidencija zaposlenih</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen, projection, tv" />
<link rel="stylesheet" href="css/style-print.css" type="text/css" media="print" />
</head>
<body>
<div id="wrapper">
</div>
  <hr class="noscreen" />
  <div class="content">
    <div class="column-left">
      <h3>ADMIN MENI</h3>
      <a href="#skip-menu" class="hidden">Skip menu</a>
      <ul class="menu">
      	<nav>
        <li><a href="<?php echo $actual_link."/administrator.php";?>">Naslovna</a></li>
        <li><a href="<?php echo $actual_link."/records.php";?>">Evidencije</a></li>
        <li><a href="<?php echo $actual_link."/users.php";?>">Korisnici</a></li>
        <li><a href="<?php echo $actual_link."/regularity.php";?>">Redovnost dolazaka</a></li>
        <li><a href="<?php echo $actual_link."/consecutive-absence.php";?>" class="active">Uzastopno odsustvo</a></li>
        <li><a href="<?php echo $actual_link."/changepassword.php";?>">Promena lozinke</a></li>
        <li><a href="<?php echo $actual_link."/logout.php";?>" class="last">Odjava</a></li>
        </nav>
      </ul>
    </div>
    <div id="skip-menu"></div>
    <div class="column-right">
      <div class="box">
        <div class="box-top"></div>
        <div class="box-in">
          <h2>Uzastopno odsustvo zaposlenih</h2>
          <table>
				<tr>
					<th>
					Ime
					</th>
					<th>
					Prezime
					</th>
					<th>
					Zvanje
					</th>
					<th>
					Poslednji dolazak
					</th>
					<th>
					Broj radnih dana nedolazaka na posao
					</th>
				</tr>
			<?php 
			$tableData = array();
			foreach($users as $user)
			{ 
				$urlPOST = $actual_link."/services/elapsed";
					$curl_post_data = array(
						'userID' => $user->get_userID()
					);
					$curl = curl_init($urlPOST);
					curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($curl, CURLOPT_POST, true);
					curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
					$result = curl_exec($curl);
					$data = json_decode($result);

					$row = array($user->jsonSerialize(),$data);
					array_push($tableData,$row);

				?>
				<tr>
					<td class="tdright">
					<?php echo $user->get_name(); ?>
					</td>			
					<td class="tdright">
					<?php echo $user->get_lastname(); ?>
					</td>
					<td class="tdright">
					<?php echo $user->get_rank(); ?>
					</td>
					<td class="tdright">
					<?php echo $data->Last; ?>
					</td>
					<td class="tdright">
					<?php echo $data->Elapsed; ?>
					</td>
				</tr>
	<?php   } 
			 $_SESSION['tableData'] = $tableData;
			 ?>
			</table> 
			<br>
			<form action="print-elapsed.php">
    			<input type="submit" value="Štampa" />
			</form>
        </div>
      </div>
    </div>
    <div class="cleaner">&nbsp;</div>
  </div>
</body>
</html>
