<?php
session_start();

if (!isset($_SESSION['username']))
{
	$_SESSION['pleaseLogin'] = true;
	header('Location: index.php');
}

require_once 'lib/utility/actualURL.php';


$urlPOST = $actual_link."/services/arrival/";
        $curl_post_data = array(
        'users_userID' => $_POST['userID'],
        'author' => 'user'
        );
        $curl = curl_init($urlPOST);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
        $succ = curl_exec($curl);

if($succ)
{
	header('Location: professor.php');
}

?>