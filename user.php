<?php
session_start();
require_once 'lib/class/User.php';

if (!isset($_SESSION['username']))
{
	$_SESSION['pleaseLogin'] = true;
	header('Location: index.php');
}

if($_SESSION['role']=='professor')
{
	header('Location: professor.php');
}
if($_SESSION['role']=='student')
{
	header('Location: student.php');
}

require_once 'lib/utility/actualURL.php';


if(isset($_POST['userID']))
{
  if($_POST['userID'] == 0)
  {
    
    $urlPOST = $actual_link."/services/adduser";
    $curl_post_data = array(
      'username' => $_POST['username'],
      'password' => $_POST['password'],
      'name' => $_POST['name'],
      'lastname' => $_POST['lastname'],
      'role' => $_POST['role'],
      'rank' => $_POST['rank'],
      'sex' => $_POST['sex']
    );
    $curl = curl_init($urlPOST);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
    $result = curl_exec($curl);
    $out = json_decode($result);

        $targetURL = $actual_link."/services/managerank";
        $curl = curl_init($targetURL);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);

    header("Location: users.php");
  }
  else
  {

    $urlPOST = $actual_link."/services/edituser";
    $curl_post_data = array(
      'userID' => $_POST['userID'],
      'username' => $_POST['username'],
      'password' => $_POST['password'],
      'name' => $_POST['name'],
      'lastname' => $_POST['lastname'],
      'role' => $_POST['role'],
      'rank' => $_POST['rank'],
      'sex' => $_POST['sex']
    );
    $curl = curl_init($urlPOST);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
    $result = curl_exec($curl);
    $out = json_decode($result);

        $targetURL = $actual_link."/services/managerank";
        $curl = curl_init($targetURL);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);

    header("Location: users.php");
  }
}


if(isset($_GET['userID']))
{
  $targetURL = $actual_link."/services/user?userID=".$_GET['userID'];
  $curl = curl_init($targetURL);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  $response = curl_exec($curl);
  $data = json_decode($response);
  $id = $_GET['userID'];
  if($data[0])
  {
    $user = new User();
    $user->jsonDeserialize($data[0]);
    $_SESSION['exists'] = true;
  }
  else
  {
    $_SESSION['exists'] = false;
  }
}
else
{
  $user = new User();
  $id = 0;
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Evidencija zaposlenih</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen, projection, tv" />
<link rel="stylesheet" href="css/style-print.css" type="text/css" media="print" />
</head>
<body>
<div id="wrapper">
</div>
  <hr class="noscreen" />
  <div class="content">
    <div class="column-left">
      <h3>ADMIN MENI</h3>
      <a href="#skip-menu" class="hidden">Skip menu</a>
      <ul class="menu">
        <nav>
        <li><a href="<?php echo $actual_link."/administrator.php";?>" >Naslovna</a></li>
        <li><a href="<?php echo $actual_link."/records.php";?>">Evidencije</a></li>
        <li><a href="<?php echo $actual_link."/users.php";?>" class="active">Korisnici</a></li>
        <li><a href="<?php echo $actual_link."/regularity.php";?>">Redovnost dolazaka</a></li>
        <li><a href="<?php echo $actual_link."/consecutive-absence.php";?>">Uzastopno odsustvo</a></li>
        <li><a href="<?php echo $actual_link."/changepassword.php";?>">Promena lozinke</a></li>
        <li><a href="<?php echo $actual_link."/logout.php";?>" class="last">Odjava</a></li>
        </nav>
      </ul>
    </div>
    <div id="skip-menu"></div>
    <div class="column-right">
      <div class="box">
        <div class="box-top"></div>
        <div class="box-in">
          <h2>Korisnik</h2>
          <?php 
    if(isset($_GET['userID']) && !$_SESSION['exists'])
    {
      echo "Korisnik sa tim ID ne postoji u bazi podataka!";
    }
    else
    {       
    ?>
    <form method="post" action="user.php">
    <table>
    <tr>
      <input type="text" name="userID" value="<?php echo $id;?>" hidden="true">
      <td>Korisničko ime: </td>
        <td><input type="text" name="username" value="<?php echo $user->get_username();?>" required></td>
      </tr>
      <tr>
      <td>Lozinka: </td>
      <td><input type="password" name="password" value="<?php echo $user->get_password(); ?>" required /></td>
      </tr>
      <tr>
      <td>Ime:</td> 
      <td><input type="text"  name="name" value="<?php echo $user->get_name(); ?>" required/></td>
      </tr>
      <tr>
      <td>Prezime:</td> 
      <td><input type="text"  name="lastname" value="<?php echo $user->get_lastname(); ?>" required/></td>
      </tr>
      <tr>
      <td>Uloga: </td>
      <td><select name="role" reuqired>
      <option value="student" <?php if ("student" == $user->get_role()) echo " selected"; ?>>Student</option>
      <option value="professor" <?php if ("professor" == $user->get_role()) echo " selected"; ?>>Profesor</option>
      <option value="administrator" <?php if ("administrator" == $user->get_role()) echo " selected"; ?>>Administrator</option>
      </select>
      </td>
      </tr>
      <tr>
      <td>Zvanje: </td>
      <td><select name="rank" >
      <option label=" "></option>
      <option value="Saradnik u nastavi" <?php if ("Saradnik u nastavi" == $user->get_rank()) echo " selected"; ?>>Saradnik u nastavi</option>
      <option value="Asistent" <?php if ("Asistent" == $user->get_rank()) echo " selected"; ?>>Asistent</option>
      <option value="Docent" <?php if ("Docent" == $user->get_rank()) echo " selected"; ?>>Docent</option>
      <option value="Vanredni profesor" <?php if ("Vanredni profesor" == $user->get_rank()) echo " selected"; ?>>Vanredni profesor</option>
      <option value="Redovni profesor" <?php if ("Redovni profesor" == $user->get_rank()) echo " selected"; ?>>Redovni profesor</option>
      </select>
      </td>
      </tr>
      <tr>
      <td>Pol:</td>
      <td><input type="radio"  name="sex" value="male" <?php if ("male" == $user->get_sex()) echo " checked"; ?>/ required> Muški</td></tr>
      <tr><td></td><td><input type="radio"  name="sex" value="female" <?php if ("female" == $user->get_sex()) echo " checked"; ?>/> Ženski</td>
      </tr>
      </table>
      <br>
      <input type="submit" value="Potvrdi">
      </form>
      <?php }     ?>
        </div>
      </div>
    </div>
    <div class="cleaner">&nbsp;</div>
  </div>
</body>
</html>